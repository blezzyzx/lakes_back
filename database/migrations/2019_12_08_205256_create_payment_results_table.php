<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_results', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pg_order_id', 255)->nullable();
            $table->string('pg_payment_id', 255)->nullable();
            $table->string('pg_amount', 255)->nullable();
            $table->string('pg_currency', 255)->nullable();
            $table->string('pg_net_amount', 255)->nullable();
            $table->string('pg_ps_amount', 255)->nullable();
            $table->string('pg_ps_full_amount', 255)->nullable();
            $table->string('pg_ps_currency', 255)->nullable();
            $table->string('pg_payment_system', 255)->nullable();
            $table->string('pg_description', 255)->nullable();
            $table->string('pg_result', 255)->nullable();
            $table->string('pg_payment_date', 255)->nullable();
            $table->string('pg_can_reject', 255)->nullable();
            $table->string('pg_user_phone', 255)->nullable();
            $table->string('pg_need_phone_notification', 255)->nullable();
            $table->string('pg_need_email_notification', 255)->nullable();
            $table->string('pg_testing_mode', 255)->nullable();
            $table->string('pg_captured', 255)->nullable();
            $table->string('pg_card_pan', 255)->nullable();
            $table->string('pg_card_exp', 255)->nullable();
            $table->string('pg_card_owner', 255)->nullable();
            $table->string('pg_auth_code', 255)->nullable();
            $table->string('pg_card_brand', 255)->nullable();
            $table->string('pg_salt', 255)->nullable();
            $table->string('pg_sig', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_results');
    }
}
