<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('lake_id')->unsigned();
            $table->foreign('lake_id')->references('id')->on('lakes');
            $table->string('lake_services', 255)->nullable();
            $table->integer('billed_amount')->nullable();
            $table->dateTime('date_from');
            $table->dateTime('date_to');
            $table->string('status')->default('reserved');
            $table->boolean('entrance_approved')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
