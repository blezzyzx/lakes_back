<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->string('parent_name', 255)->nullable();
            $table->string('car_model', 255)->nullable();
            $table->string('car_number', 255)->nullable();
            $table->string('phone_number', 11)->unique();
            $table->string('password', 255);
            $table->boolean('approve_status')->default(1);
            $table->integer('visit_count')->unsigned()->default(0);
            $table->string('document_number', 12)->unique();
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
