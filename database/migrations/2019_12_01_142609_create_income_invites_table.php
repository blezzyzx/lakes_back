<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomeInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('income_invites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_id')->unsigned();
            $table->foreign('reservation_id')->references('id')->on('reservations');
            $table->string('lake_name', 255);
            $table->dateTime('date_from');
            $table->integer('inviter')->unsigned();
            $table->foreign('inviter')->references('id')->on('users');
            $table->string('inviter_first_name', 255);
            $table->string('inviter_last_name', 255);
            $table->string('invited', 11);
            $table->foreign('invited')->references('phone_number')->on('users');
            $table->integer('outcome_invite')->unsigned();
            $table->foreign('outcome_invite')->references('id')->on('outcome_invites')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('income_invites');
    }
}
