<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutcomeInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outcome_invites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_id')->unsigned();
            $table->foreign('reservation_id')->references('id')->on('reservations');
            $table->integer('inviter')->unsigned();
            $table->foreign('inviter')->references('id')->on('users');
            $table->string('invited', 11);
            $table->string('invited_name', 255);
            $table->string('lake_name', 255);
            $table->foreign('invited')->references('phone_number')->on('users');
            $table->string('status', 10)->default('awaiting');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outcome_invites');
    }
}
