<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentFailuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_failures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pg_order_id', 255)->nullable();
            $table->string('pg_payment_id', 255)->nullable();
            $table->string('pg_salt', 255)->nullable();
            $table->string('pg_sig', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_failures');
    }
}
