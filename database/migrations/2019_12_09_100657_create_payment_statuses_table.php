<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pg_status', 255)->nullable();
            $table->string('pg_payment_id', 255)->nullable();
            $table->string('pg_transaction_status', 255)->nullable();
            $table->string('pg_can_reject', 255)->nullable();
            $table->string('pg_testing_mode', 255)->nullable();
            $table->string('pg_captured', 255)->nullable();
            $table->string('pg_card_pan', 255)->nullable();
            $table->string('pg_create_date', 255)->nullable();
            $table->string('pg_salt', 255)->nullable();
            $table->string('pg_sig', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_statuses');
    }
}
