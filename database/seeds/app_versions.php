<?php

use Illuminate\Database\Seeder;

class app_versions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('app_versions')->insert([
    		[
    			'ios' => '1.0.0',
    			'android' => '1.0.0',
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    	]);
    }
}
