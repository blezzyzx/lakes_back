<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(roles::class);
        $this->call(users::class);
        $this->call(file_categories::class);
        $this->call(lakes::class);
        $this->call(services::class);
        $this->call(lake_services::class);
        $this->call(app_versions::class);
    }
}
