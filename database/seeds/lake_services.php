<?php

use Illuminate\Database\Seeder;

class lake_services extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lake_services')->insert([
    		[
    			'lake_id' => '1',
    			'service_id' => '3',
                'price' => 800,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'lake_id' => '1',
    			'service_id' => '2',
                'price' => 700,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'lake_id' => '1',
    			'service_id' => '4',
                'price' => 700,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'lake_id' => '2',
    			'service_id' => '3',
                'price' => 800,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'lake_id' => '2',
    			'service_id' => '2',
                'price' => 700,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'lake_id' => '2',
    			'service_id' => '4',
                'price' => 700,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'lake_id' => '2',
    			'service_id' => '1',
                'price' => 700,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'lake_id' => '3',
    			'service_id' => '4',
                'price' => 700,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'lake_id' => '3',
    			'service_id' => '1',
                'price' => 700,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'lake_id' => '4',
    			'service_id' => '4',
                'price' => 700,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'lake_id' => '4',
    			'service_id' => '1',
                'price' => 700,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    	]);
    }
}