<?php

use Illuminate\Database\Seeder;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
    		[
    			'first_name' => 'admin',
    			'last_name' => 'admin',
    			'parent_name' => 'admin',
    			'document_number' => 'admin',
                'car_model' => 'toyota 20',
                'car_number' => 'f210awp',
                'phone_number' => '77776665541',
    			'role_id' => 1,
    			'approve_status' => 1,
    			'visit_count' => 2,
    			'password' => Hash::make('123'),
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'first_name' => 'user',
    			'last_name' => 'user',
    			'parent_name' => 'user',
    			'document_number' => 'user',
                'car_model' => 'toyota 20',
                'car_number' => 'f210awp',
                'phone_number' => '77776665531',
    			'role_id' => 2,
    			'approve_status' => 1,
    			'visit_count' => 2,
    			'password' => Hash::make('123'),
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'first_name' => 'manager',
    			'last_name' => 'manager',
    			'parent_name' => 'manager',
    			'document_number' => 'manager',
                'car_model' => 'toyota 20',
                'car_number' => 'f210awp',
                'phone_number' => '77776665532',
    			'role_id' => 3,
    			'approve_status' => 1,
    			'visit_count' => 2,
    			'password' => Hash::make('123'),
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
            [
                'first_name' => 'Gulshat',
                'last_name' => 'Gulshat',
                'parent_name' => 'Gulshat',
                'document_number' => '177473850237',
                'car_model' => 'toyota 20',
                'car_number' => 'f210awp',
                'phone_number' => '77473850237',
                'role_id' => 1,
                'approve_status' => 1,
                'visit_count' => 2,
                'password' => Hash::make('123456'),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'first_name' => 'Kirill',
                'last_name' => 'Kirill',
                'parent_name' => 'Kirill',
                'document_number' => '12345',
                'car_model' => 'toyota 20',
                'car_number' => 'f210awp',
                'phone_number' => '77711487110',
                'role_id' => 1,
                'approve_status' => 1,
                'visit_count' => 2,
                'password' => Hash::make('qwe123'),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'first_name' => 'Kostya',
                'last_name' => 'Kostya',
                'parent_name' => 'Kostya',
                'document_number' => 'admin7',
                'car_model' => 'toyota 20',
                'car_number' => 'f210awp',
                'phone_number' => '77072301555',
                'role_id' => 1,
                'approve_status' => 1,
                'visit_count' => 2,
                'password' => Hash::make('123456'),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'first_name' => 'Aman',
                'last_name' => 'Aman',
                'parent_name' => 'Aman',
                'document_number' => 'aman',
                'car_model' => 'toyota 20',
                'car_number' => 'f210awp',
                'phone_number' => '77776665545',
                'role_id' => 1,
                'approve_status' => 1,
                'visit_count' => 2,
                'password' => Hash::make('123'),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
    	]);
    }
}
