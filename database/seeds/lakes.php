<?php

use Illuminate\Database\Seeder;

class lakes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lakes')->insert([
    		[
    			'name' => 'Исей',
                'description' => 'Озеро располагается в самом центре заповедника и является наиболее красивым и чистым!',
                'price' => 1000,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'name' => 'Султанкельди',
                'description' => 'Озеро располагается в самом центре заповедника и является наиболее красивым и чистым!',
                'price' => 1000,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'name' => 'Кокай',
                'description' => 'Озеро располагается в самом центре заповедника и является наиболее красивым и чистым!',
                'price' => 1000,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'name' => 'Асаубалык',
                'description' => 'Озеро располагается в самом центре заповедника и является наиболее красивым и чистым!',
                'price' => 1000,
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    	]);
    }
}
