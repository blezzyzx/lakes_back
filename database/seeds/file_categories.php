<?php

use Illuminate\Database\Seeder;

class file_categories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('file_categories')->insert([
    		[
    			'name' => 'Переднее фото удостоверение',
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    		[
    			'name' => 'Заднее фото удостоверение',
    			'created_at' => \Carbon\Carbon::now(),
    			'updated_at' => \Carbon\Carbon::now()
    		],
    	]);
    }
}
