<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.t_text{
			vertical-align: bottom;
			font-weight:bold;
			padding-bottom: 0px;
			line-height: .8em;
		}
		.ds{
			vertical-align: bottom;
			font-weight:bold;
			padding-bottom: 0px;
			margin-bottom: 0px;
			line-height: .8em;
			font-size:20px;
			padding-left:20px;
		}
		.in{
			font-size:22px;
			padding-left: 5px;
			background: #ffffff;
		}
		.table{
			background: #cccccc;
		}
		body{
			font-family:'Arial';
		}
	</style>
</head>
<body>
	<table align="center" style="width: 700px;margin-bottom: 10px;">
		<tr>
			<td class="t_text" style="font-size:18px;">ЭЛЕКТРОННЫЙ БИЛЕТ</td>
			<td class="t_text" style="color:#777777; font-size:12px;text-align: right;">РАСПЕЧАТАЙТЕ ИЛИ ПОКАЖИТЕ ЭЛЕКТРОННЫЙ БИЛЕТ</td>
		</tr>
	</table>
	<table align="center" class="table" cellspacing="10px" style="width: 700px;">
		<tr>
			<td class="ds" style="width: 300px;">Озеро</td>
			<td class="ds" style="width: 300px;">Услуги</td>
			<td class="ds" style="width: 300px;">Дата Номер заказа</td>
			<td class="ds" style="width: 300px;">Цена билета</td>
		</tr>
		<?php
		$date_from = date_format(date_create($reservation->date_from),"d/m/Y");
		echo '
		<tr>
		<td class="in">'.$reservation->lake->name.'</td>
		<td class="in">'.$services.'</td>
		<td class="in">'.$date_from.'<br>№'.sprintf('%06d', $reservation->id).'</td>
		<td class="in">'.$reservation->billed_amount.'</td>
		</tr>';
		?>
		<tr>
			<td class="ds" style="width: 300px;">Фамилия Имя Отчество</td>
			<td class="ds" style="width: 300px;">Мобильный телефон</td>
			<td class="ds" style="width: 300px;">Модель машины<br>Номер машины</td>
			<td class="ds" style="width: 300px;">Иин</td>
		</tr>
		<?php
		echo '
		<tr>
		<td class="in">'.$user->last_name.' '.$user->first_name.' '.$user->parent_name.'</td>
		<td class="in">'.$user->phone_number.'</td>
		<td class="in">'.$user->car_model.'<br>'.$user->car_number.'</td>
		<td class="in">'.$user->document_number.'</td>
		</tr>';
		?>
		<tr>
			<td colspan="4" class="ds" style="text-align: center;">Участники</td>
		</tr>
		<?php 
		foreach ($invited_users as $i_user) {
			echo '
			<tr>
			<td class="in">'.$i_user->last_name.' '.$i_user->first_name.' '.$i_user->parent_name.'</td>
			<td class="in">'.$i_user->phone_number.'</td>
			<td class="in">'.$i_user->car_model.'<br>'.$user->car_number.'</td>
			<td class="in">'.$i_user->document_number.'</td>
			</tr>';
		}
		?>
	</table>
	<table align="center" style="width: 700px;margin-bottom: 10px;">
		<tr>
			<td style="text-align: center;padding-top: 20px;">
				<barcode code="{{ implode(' ', ['https://manager.zapovednik-korgalzhyn.kz/pay/'.$reservation->id]) }}" type="QR" class="barcode" size="2" height="2" error="M" />
			</td>
		</tr>
		<tr>
			<td style="text-align: center;color:#777777;padding-top: 10px;">
				ДЛЯ ПОДТВЕРЖДЕНИЯ БИЛЕТА <br> ПОКАЖИТЕ QR КОД КАССИРУ
			</td>
		</tr>
	</table>
</body>
</html>