<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/payment_status', 'ReservationsController@payment_status');
Route::get('/success', 'ReservationsController@success_result');
Route::post('/success', 'ReservationsController@success_result');
Route::get('/failure', 'ReservationsController@failure_result');
Route::post('/failure', 'ReservationsController@failure_result');
Route::get('/process_result', 'ReservationsController@process_result');
Route::post('/process_result', 'ReservationsController@process_result');

Route::post('login', 'Auth\AccessTokenController@issueToken');
Route::post('logout', 'Auth\LoginController@logout');
Route::post('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@create'])->middleware('cors');
Route::group(['prefix' => 'document'], function () {
        Route::post('/upload', 'FilesController@upload');
        Route::post('/delete', 'FilesController@delete');
});
Route::get('not_auth', function(){
	return response()->json(['message' => 'Вы не авторизованы.'],401);
})->name('not_auth');

Route::group(['middleware' => 'auth:api'], function () {
  Route::post('/payment_url', 'ReservationsController@payment_url');
  Route::post('/test_payment_url', 'ReservationsController@test_payment_url');
	Route::post('/users/search', 'UsersController@search')->middleware('role');
	Route::get('/user', 'UsersController@user');

    Route::group(['prefix' => 'app_versions'], function () {
        Route::post('/update', 'LakesController@updateVersion')->middleware('role');
    });

    Route::group(['prefix' => 'lakes'], function () {
    	Route::get('/all', 'LakesController@all');
        Route::get('/all_with_ver', 'LakesController@all_with_ver');
        Route::post('/create', 'LakesController@create')->middleware('admin');
        Route::post('/update', 'LakesController@update')->middleware('admin');
        Route::post('/delete', 'LakesController@delete')->middleware('admin');
        Route::post('/add_service', 'LakesController@add_service')->middleware('admin');
        Route::post('/update_service', 'LakesController@update_service')->middleware('admin');
        Route::post('/delete_service', 'LakesController@delete_service')->middleware('admin');
    });

    Route::group(['prefix' => 'services'], function () {
    	Route::get('/all', 'ServicesController@all');
        Route::post('/create', 'ServicesController@create')->middleware('admin');
        Route::post('/update', 'ServicesController@update')->middleware('admin');
        Route::post('/delete', 'ServicesController@delete')->middleware('admin');
    });

    Route::group(['prefix' => 'reservations'], function () {
        Route::post('/index', 'ReservationsController@index')->middleware('role');
        Route::post('/entrance_approve', 'ReservationsController@entrance_approve')->middleware('role');
    	  Route::post('/all', 'ReservationsController@all')->middleware('approved');
        Route::post('/test_all', 'ReservationsController@test_all')->middleware('approved');
        Route::post('/create', 'ReservationsController@create')->middleware('approved');
        Route::post('/update', 'ReservationsController@update')->middleware('approved');
        Route::post('/delete', 'ReservationsController@delete')->middleware('approved');
        Route::post('/pdf', 'ReservationsController@pdf')->middleware('approved');
    });

    Route::group(['prefix' => 'income_invites'], function () {
    	Route::get('/all', 'IncomeInvitesController@all')->middleware('approved');
        Route::post('/answer', 'IncomeInvitesController@answer')->middleware('approved');
    });

    Route::group(['prefix' => 'outcome_invites'], function () {
    	Route::post('/all', 'OutcomeInvitesController@all')->middleware('approved');
        Route::post('/create', 'OutcomeInvitesController@create')->middleware('approved');
        Route::post('/delete', 'OutcomeInvitesController@delete')->middleware('approved');
        Route::post('/delete_altrv', 'OutcomeInvitesController@delete_altrv')->middleware('approved');
    });
});
