<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## For front-enders

Postman collection of the API:
<a href="LAKES_API.json" download>LAKES_API.json</a>
```
client_secret = "yLjZHV7hywUDgtecYFwO6ygSMx3S1BOpegIrLepY"
```

## Prerequisites

LAMP for laravel 5.7

## Installation

```
git clone
cd LAKES_BACK
composer install
cp .env_local .env
php artisan migrate --seed
php artisan passport:install
php artisan storage:link
sudo chown -R www-data:www-data storage
sudo chmod -R 777 storage
sudo chmod 777 ./vendor/mpdf/mpdf/tmp/
file permission: stackoverflow.com/questions/30639174/how-to-set-up-file-permissions-for-laravel

add to /var/www/lakes_back/vendor/zendframework/zend-diactoros/src/ServerRequest.php
public function setSukaBlyat($key, $value)
    {
        return $this->parsedBody[$key] = $value;
    }
```

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
