<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMIN           = 1;
    const USER            = 2;
    const MANAGER         = 3;

    /**
     * Get users
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
