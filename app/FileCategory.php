<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileCategory extends Model
{
    protected $table = "file_categories";

    const doc_front	= 1;
    const doc_back	= 2;

    public function file()
    {
        return $this->belongsTo(File::class, 'category_id');
    }
}
