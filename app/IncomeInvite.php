<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Reservation;

class IncomeInvite extends Model
{
    protected $fillable = [
        'reservation_id', 'inviter', 'invited', 'outcome_invite', 'inviter_first_name', 'inviter_last_name', 'lake_name', 'date_from'
    ];

    public function outcome_invite()
    {
        return $this->hasOne(OutcomeInvite::class, 'id', 'outcome_invite');
    }
}
