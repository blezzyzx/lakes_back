<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentFailure extends Model
{
    protected $fillable = [
        'pg_order_id', 'pg_payment_id', 'pg_salt', 'pg_sig',
    ];
}
