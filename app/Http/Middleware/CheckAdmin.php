<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()) {
            return response()->json(['message' => 'Вы не авторизованы.'],401);
        }
        $role = Auth::user()->role;
        if ($role->name == 'Admin') {
            return $next($request);
        }

        return response()->json(['message' => 'У вас недостаточно прав.'],403);
    }
}
