<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserApproved
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()) {
            return response()->json(['message' => 'Вы не авторизованы.'],401);
        }
        if (Auth::user()->approve_status == 1) {
            return $next($request);
        }

        return response()->json(['message' => 'Ваша регистрация ожидает подтверждения. Бронирование недоступно.'],403);
    }
}
