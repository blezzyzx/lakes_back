<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\File;
use App\FileCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
  public function check_number($phone_number){
    $phone_number = trim($phone_number);
    if($phone_number[0] == '+'){
      $phone_number = substr($phone_number, 1);
    }
    if(strlen($phone_number) == 11){
      $phone_number = substr($phone_number, 1);
    }
    $phone_number = '7'.$phone_number;
    if(strlen($phone_number) != 11){
      return false;
    }
    return $phone_number;
  }

  public function search(Request $request) {
    $request = $request->all();
    $phone_number = $this->check_number($request['phone_number']);
    if(!$phone_number){
      return response()->json(['message' => ['Не правильный номер.']], 400);
    }

    $user = User::where('phone_number', $phone_number)->first();
    if (!$user) {
      return response()->json(['message' => ['Пользователь не найден.']], 400);
    }
    $doc_front = File::where(['user_id' => $user->id,
      'category_id' => FileCategory::doc_front])->first();
    $doc_back = File::where(['user_id' => $user->id,
      'category_id' => FileCategory::doc_back])->first();
    if($doc_front){
      $user['doc_front_url'] = $doc_front->url;
    }
    if($doc_back){
      $user['doc_back_url'] = $doc_back->url;
    }
    return response()->json($user, 200);
  }

  public function user() {
   $user = User::where('id', Auth::user()->id)->first();
   if (!$user) {
    return response()->json(['message' => ['Пользователь не найден.']], 400);
  }
  $doc_front = File::where(['user_id' => $user->id,
    'category_id' => FileCategory::doc_front])->first();
  $doc_back = File::where(['user_id' => $user->id,
    'category_id' => FileCategory::doc_back])->first();
  if($doc_front){
    $user['doc_front_url'] = $doc_front->url;
  }
  if($doc_back){
    $user['doc_back_url'] = $doc_back->url;
  }
  return response()->json($user, 200);
}
}
