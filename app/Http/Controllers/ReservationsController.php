<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Reservation;
use App\LakeServices;
use App\User;
use App\Lake;
use App\Service;
use App\PaymentResult;
use App\PaymentSuccess;
use App\PaymentFailure;
use App\PaymentStatus;
use App\OutcomeInvite;
use Illuminate\Support\Facades\Validator;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;

class ReservationsController extends Controller
{
	public function cmp($a, $b)
	{
		return strcmp($b->created_at, $a->created_at);
	}
	public function all(Request $request)
	{
		try {
			$user_id = Auth::user()->id;
			$items = Reservation::where([
				['user_id', $user_id],
				['status', 'paid']
			])
			->with('lake', 'outcome_invites')->get();

			$items = $items->filter(function ($item) {
				return $item['lake'] != null;
			});

			$items = $items->map(function($item){
				$item->lake_services = json_decode($item->lake_services);
				$services = [];
				foreach ($item->lake_services as $ls_id) {
					$service_id = LakeServices::where('id', $ls_id)->first()->service_id;
					$services[] = Service::where('id', $service_id)->first();
				}
				$item['services'] = $services;
				return $item;
			});

			$reservations = [];
			$reservations = array_merge($reservations, $items->all());
			$all_reservs = Reservation::where('status', 'paid')->get();
			foreach ($all_reservs as $reserv) {
				foreach ($reserv->outcome_invites as $o_invite) {
					$user = User::where('phone_number', $o_invite->invited)->first();
					if($user->id == $user_id){
						$reservations[] = $this->my_reservs($reserv->id);
					}
				}
			}
			usort($reservations, array($this, "cmp"));
			return response()->json($reservations, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorInd(array $data)
	{
		return Validator::make($data, [
			'reservation_id' => ['required', 'integer'],
		]);
	}
	public function index(Request $request)
	{
		$request = $request->all();
		$valid = $this->validatorInd($request);
		if ($valid->fails()) {
			$jsonError = response()->json(['message' => $valid->errors()->all()], 400);
			return $jsonError;
		}
		try {
			$item = Reservation::where('id', $request['reservation_id'])
			->with('lake', 'outcome_invites')->first();
			if(empty($item)){
				return response()->json(['message' => 'Резерв не найден'], 400);
			}
			if($item->entrance_approved == 1){
				return response()->json(['message' => 'Резерв уже был подтвержден.'], 400);
			}
			$item->lake_services = json_decode($item->lake_services);
			$services = [];
			$invited_users = [];
			foreach ($item->outcome_invites as $o_invite) {
				$invited_users[] = User::where('phone_number', $o_invite->invited)->first();
			}
			$buyer_user = User::where('id', $item->user_id)->first();
			foreach ($item->lake_services as $ls_id) {
				$service_id = LakeServices::where('id', $ls_id)->first()->service_id;
				$services[] = Service::where('id', $service_id)->first();
			}
			$item['services'] = $services;
			$item['invited_users'] = $invited_users;
			$item['buyer_user'] = $buyer_user;
			return response()->json($item, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorApp(array $data)
	{
		return Validator::make($data, [
			'reservation_id' => ['required', 'integer'],
		]);
	}
	public function entrance_approve(Request $request)
	{
		$request = $request->all();
		$valid = $this->validatorApp($request);
		if ($valid->fails()) {
			$jsonError = response()->json(['message' => $valid->errors()->all()], 400);
			return $jsonError;
		}
		try {
			$item = Reservation::where('id', $request['reservation_id'])->first();
			if(empty($item)){
				return response()->json(['message' => 'Резерв не найден'], 400);
			}
			$item->entrance_approved = 1;
			$item->save();
			return response()->json($item, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	public function my_reservs($reserv_id){
		try {
			$item = Reservation::where('id', $reserv_id)
			->with('lake', 'outcome_invites')->first();
			$item->lake_services = json_decode($item->lake_services);
			$services = [];
			foreach ($item->lake_services as $ls_id) {
				$service_id = LakeServices::where('id', $ls_id)->first()->service_id;
				$services[] = Service::where('id', $service_id)->first();
			}
			$item['services'] = $services;
			return $item;
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validator(array $data)
	{
		return Validator::make($data, [
			'lake_id' => ['required', 'integer'],
			'lake_services' => ['nullable', 'array'],
			'lake_services.*' => ['nullable', 'integer'],
			'date_from' => ['required', 'date'],
			'date_to' => ['required', 'date'],
		]);
	}
	public function create(Request $request) {
		$request = $request->all();
		$valid = $this->validator($request);
		if ($valid->fails()) {
			$jsonError = response()->json(['message' => $valid->errors()->all()], 400);

			return $jsonError;
		}
		DB::beginTransaction();
		try{
			$lake = Lake::where('id', $request['lake_id'])->first();
			$services = [];
			foreach ($request['lake_services'] as $lake_service) {
				$ls = LakeServices::where('id', $lake_service)->first();
				if(empty($ls)){
					return response()->json(['message' => ['Услуга '.$lake_service.' не найдена.']], 400);
				}
				$services[] = Service::where('id', $ls->service_id)->first();
			}
			$my_reservs = Reservation::where([
				['user_id', '=', Auth::user()->id],
				['lake_id', '=', $request['lake_id']],
				['status', '=', 'paid']
			])->get();
			foreach ($my_reservs as $reserv) {
				$t_from = strtotime($reserv->date_from);
				$t_to = strtotime($reserv->date_to);
				$req_from = strtotime($request['date_from']);
				$req_to = strtotime($request['date_to']);

				if(($req_from >= $t_from && $req_from <= $t_to) || ($req_to >= $t_from && $req_to <= $t_to)){
					return response()->json(['message' => ['В эти даты у вас есть резерв.']], 400);
				}
			}

			$reservation = new Reservation();
			$reservation->user_id = Auth::user()->id;
			$reservation->lake_id = $request['lake_id'];
			$reservation->lake_services = json_encode($request['lake_services']);
			$reservation->date_from = $request['date_from'];
			$reservation->date_to = $request['date_to'];
			$reservation->save();

			$result = $reservation;
			$result['lake'] = $lake;
			$result['services'] = $services;
			$result['lake_services'] = $request['lake_services'];
			DB::commit();
			return response()->json($result, 200);
		}
		catch (\Exception $e) {
			DB::rollback();
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorU(array $data)
	{
		return Validator::make($data, [
			'id' => ['required', 'integer'],
			'lake_services' => ['nullable', 'array'],
			'lake_services.*' => ['nullable', 'integer'],
			'date_from' => ['required', 'date'],
			'date_to' => ['required', 'date'],
		]);
	}
	public function update(Request $request) {
		$request = $request->all();
		$valid = $this->validatorU($request);
		if ($valid->fails()) {
			$jsonError = response()->json(['message' => $valid->errors()->all()], 400);

			return $jsonError;
		}
		DB::beginTransaction();
		try{
			$reservation = Reservation::where('id', $request['id'])->first();
			if($reservation->user_id != Auth::user()->id){
				return response()->json(['message' => ['Резерв вам не принадлежит.']], 400);
			}
			if($reservation->status == 'paid'){
				return response()->json(['message' => ['Резерв уже оплачен.']], 400);
			}
			$reservation->lake_services = json_encode($request['lake_services']);
			$lake = Lake::where('id', $reservation['lake_id'])->first();
			$services = [];
			foreach ($request['lake_services'] as $lake_service) {
				$ls = LakeServices::where('id', $lake_service)->first();
				if(empty($ls)){
					return response()->json(['message' => ['Услуга не найдена.']], 400);
				}
				$services[] = Service::where('id', $ls->service_id)->first();
			}
			$reservation->date_from = $request['date_from'];
			$reservation->date_to = $request['date_to'];
			$reservation->save();
			DB::commit();
			$reservation['services'] = $services;
			$reservation['lake'] = $lake;
			return response()->json($reservation, 200);
		}
		catch (\Exception $e) {
			DB::rollback();
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorD(array $data)
	{
		return Validator::make($data, [
			'id' => ['required', 'integer'],
		]);
	}
	public function delete(Request $request) {
		$request = $request->all();
		$valid = $this->validatorD($request);
		DB::beginTransaction();
		if ($valid->fails()) {
			$jsonError = response()->json(['message' => $valid->errors()->all()], 400);

			return $jsonError;
		}
		try{
			$reservation = Reservation::where('id', $request['id'])->first();
			if($reservation->user_id != Auth::user()->id){
				return response()->json(['message' => ['Резерв вам не принадлежит.']], 400);
			}
			if($reservation->status == 'paid'){
				return response()->json(['message' => ['Резерв уже оплачен.']], 400);
			}

			if($reservation->delete()){
				$message = 'Запись была удалена!';
			}else{
				$message = 'Запись не была удалена!';
			}
			DB::commit();
			return response()->json(['message' => [$message]], 200);
		}
		catch (\Exception $e) {
			DB::rollback();
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorPdf(array $data)
	{
		return Validator::make($data, [
			'reservation_id' => ['required', 'integer'],
		]);
	}
	public function pdf(Request $request) {
		$request = $request->all();
		$valid = $this->validatorPdf($request);
		if ($valid->fails()) {
			$jsonError = response()->json(['message' => $valid->errors()->all()], 400);
			return $jsonError;
		}
		$user = Auth::user();
		$reservation = Reservation::where('id', $request['reservation_id'])
		->with('outcome_invites')->first();
		if($reservation->user_id != $user->id){
			$p_check = false;
			foreach ($reservation->outcome_invites as $o_invite) {
				if($o_invite->invited == $user->phone_number){
					$p_check = true;
					break;
				}
			}
			if(!$p_check){
				return response()->json(['message' => ['Резерв вам не принадлежит.']], 400);
			}
		}
		if($reservation->status != 'paid'){
			return response()->json(['message' => ['Резерв не оплачен.']], 400);
		}
		$services = '';
		$lss = json_decode($reservation->lake_services);
		$ch = false;
		foreach ($lss as $ls) {
			$serv_id = LakeServices::where('id', $ls)->first()->service_id;
			$serv = Service::where('id', $serv_id)->first();
			if(empty($serv)){
				return response()->json(['message' => ['Услуга '.$ls.' не найдена.']], 400);
			}
			if($ch){
				$services = $services.'<br>- '.$serv->name;
			}else{
				$services = '- '.$serv->name;
				$ch = true;
			}
		}
		$invited_users = [];
		foreach ($reservation->outcome_invites as $o_invite) {
			$invited_users[] = User::where('phone_number', $o_invite->invited)->first();
		}
		$buyer_user = User::where('id', $reservation->user_id)->first();
		try{
			$content = view('check', ['user' => $buyer_user, 'reservation' => $reservation, 'services' => $services, 'invited_users' => $invited_users])->render();
			$mpdf = new Mpdf();
			$mpdf->WriteHTML($content);
			$location = '/var/www/lakes_back/public/storage/pdfs/';
			$name = 'Ticket'.$reservation->id.'.pdf';
			$path = $location.$name;
			$url = 'https://api.zapovednik-korgalzhyn.kz/storage/pdfs/'.$name;
			$mpdf->Output($path, \Mpdf\Output\Destination::FILE);
            // $file = base64_encode($mpdf->Output('', Destination::STRING_RETURN));
			return response()->json(['url' => $url], 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorUrl(array $data)
	{
		return Validator::make($data, [
			'reservation_id' => ['required', 'integer'],
		]);
	}
	public function payment_url(Request $request) {
		DB::beginTransaction();
		try{
			$request = $request->all();
			$valid = $this->validatorUrl($request);
			if ($valid->fails()) {
				$jsonError = response()->json(['message' => $valid->errors()->all()], 400);
				return $jsonError;
			}
			$user = Auth::user();
			$reservation = Reservation::where('id', $request['reservation_id'])
			->with('outcome_invites')->first();
			if($reservation->user_id != $user->id){
				return response()->json(['message' => ['Резерв вам не принадлежит.']], 400);
			}
			if($reservation->status == 'paid'){
				return response()->json(['message' => ['Резерв уже оплачен.']], 400);
			}
			foreach ($reservation['outcome_invites'] as $o_invites) {
				if($o_invites['status'] != 'accepted'){
					return response()->json(['message' => ['Не все приглашения приняты.']], 400);
				}
			}

			$billed = 0;
			$lake = Lake::where('id', $reservation['lake_id'])->first();
			$billed += $lake->price*(sizeof($reservation['outcome_invites'])+1);
			foreach (json_decode($reservation['lake_services']) as $lake_service) {
				$ls = LakeServices::where('id', $lake_service)->first();
				if(empty($ls)){
					return response()->json(['message' => ['Услуга '.$lake_service.' не найдена.']], 400);
				}
				$billed += $ls->price;
			}
			$reservation->billed_amount = $billed;
			$reservation->save();

			$request = [
				'pg_merchant_id'=> 522248,
				'pg_order_id'=> $reservation->id,
				'pg_amount' => $reservation->billed_amount,
				'pg_currency' => 'KZT',
				'pg_request_method' => 'POST',
				'pg_result_url' => 'https://api.zapovednik-korgalzhyn.kz/api/process_result',
				'pg_success_url' => 'https://api.zapovednik-korgalzhyn.kz/api/success',
				'pg_success_url_method' => 'AUTOGET',
				'pg_failure_url' => 'https://api.zapovednik-korgalzhyn.kz/api/failure',
				'pg_failure_url_method' => 'AUTOGET',
				'pg_description' => $reservation->lake->name,
				'pg_user_phone' => $user->phone_number,
				'pg_language' => 'ru',
				'pg_salt' => 'zapovednik'.microtime(),
			];

		// $request['pg_testing_mode'] = 1; //add this parameter to request for testing payments

		//if you pass any of your parameters, which you want to get back after the payment, then add them. For example:
		// $request['client_name'] = 'My Name';
		// $request['client_address'] = 'Earth Planet';
		//generate a signature and add it to the array
		ksort($request); //sort alphabetically
		array_unshift($request, 'payment.php');
		array_push($request, 'IPwavLfyKH3RT1yj'); //add your secret key (you can take it in your personal cabinet on paybox system)

		$request['pg_sig'] = md5(implode(';', $request));
		unset($request[0], $request[1]);
		$query = http_build_query($request);
		DB::commit();
		return response()->json(['url' => 'https://api.paybox.money/payment.php?'.$query], 200);
	}
	catch (\Exception $e) {
		DB::rollback();
		return response()->json(['message' => $e->getMessage()], 500);
	}
}

public function process_result(Request $request) {
	DB::beginTransaction();
	try{
		$request = $request->all();
		$payment_result = new PaymentResult();
		$payment_result->pg_order_id = $request['pg_order_id'] ?? null;
		$payment_result->pg_payment_id = $request['pg_payment_id'] ?? null;
		$payment_result->pg_amount = $request['pg_amount'] ?? null;
		$payment_result->pg_currency = $request['pg_currency'] ?? null;
		$payment_result->pg_net_amount = $request['pg_net_amount'] ?? null;
		$payment_result->pg_ps_amount = $request['pg_ps_amount'] ?? null;
		$payment_result->pg_ps_full_amount = $request['pg_ps_full_amount'] ?? null;
		$payment_result->pg_ps_currency = $request['pg_ps_currency'] ?? null;
		$payment_result->pg_payment_system = $request['pg_payment_system'] ?? null;
		$payment_result->pg_description = $request['pg_description'] ?? null;
		$payment_result->pg_result = $request['pg_result'] ?? null;
		$payment_result->pg_payment_date = $request['pg_payment_date'] ?? null;
		$payment_result->pg_can_reject = $request['pg_can_reject'] ?? null;
		$payment_result->pg_user_phone = $request['pg_user_phone'] ?? null;
		$payment_result->pg_need_phone_notification = $request['pg_need_phone_notification'] ?? null;
		$payment_result->pg_need_email_notification = $request['pg_need_email_notification'] ?? null;
		$payment_result->pg_testing_mode = $request['pg_testing_mode'] ?? null;
		$payment_result->pg_captured = $request['pg_captured'] ?? null;
		$payment_result->pg_card_pan = $request['pg_card_pan'] ?? null;
		$payment_result->pg_card_exp = $request['pg_card_exp'] ?? null;
		$payment_result->pg_card_owner = $request['pg_card_owner'] ?? null;
		$payment_result->pg_auth_code = $request['pg_auth_code'] ?? null;
		$payment_result->pg_card_brand = $request['pg_card_brand'] ?? null;
		$payment_result->pg_salt = $request['pg_salt'] ?? null;
		$payment_result->pg_sig = $request['pg_sig'] ?? null;
		$payment_result->save();

		if($request['pg_result'] == 1){
				//POST STATUS
			if($this->payment_status($request) == false){
				return response()->json(['message' => 'Ошибка проверки статуса.'], 400);
			}
			$reservation = Reservation::where('id', $request['pg_order_id'])->first();
			if(empty($reservation)){
				return response()->json(['message' => ['Резерв не найден.']], 400);
			}
			$reservation->status = 'paid';
			$user = User::where('id', $reservation->user_id)->first();
			$user->visit_count += 1;
			$user->save();
			foreach ($reservation->outcome_invites as $o_invite) {
				$user = User::where('phone_number', $o_invite->invited)->first();
				$user->visit_count += 1;
				$user->save();
			}
			$reservation->save();
		}
		DB::commit();
		return response()->json(['message' => 'Результат получен.'], 200);
	}
	catch (\Exception $e) {
		DB::rollback();
		return response()->json(['message' => $e->getMessage()], 500);
	}
}

public function success_result(Request $request) {
	$request = $request->all();
	$payment_success = new PaymentSuccess();
	$payment_success->pg_order_id = $request['pg_order_id'] ?? null;
	$payment_success->pg_payment_id = $request['pg_payment_id'] ?? null;
	$payment_success->pg_salt = $request['pg_salt'] ?? null;
	$payment_success->pg_sig = $request['pg_sig'] ?? null;
	$payment_success->save();
	return response()->json(['message' => 'Успешно оплачено.'], 200);
}

public function failure_result(Request $request) {
	$request = $request->all();
	$payment_failure = new PaymentFailure();
	$payment_failure->pg_order_id = $request['pg_order_id'] ?? null;
	$payment_failure->pg_payment_id = $request['pg_payment_id'] ?? null;
	$payment_failure->pg_salt = $request['pg_salt'] ?? null;
	$payment_failure->pg_sig = $request['pg_sig'] ?? null;
	$payment_failure->save();
	return response()->json(['message' => 'Ошибка оплаты.'], 200);
}

public function payment_status(array $data) {
	try{
		$request = [
			'pg_merchant_id'=> 522248,
			'pg_payment_id' => $data['pg_payment_id'],
			'pg_order_id'=> $data['pg_order_id'],
			// 'pg_payment_id' => 110797940,
			// 'pg_order_id'=> 4,
			'pg_salt' => 'zapovednik'.microtime(),
		];
		ksort($request);
		array_unshift($request, 'get_status.php');
		array_push($request, 'IPwavLfyKH3RT1yj');
		$request['pg_sig'] = md5(implode(';', $request));
		unset($request[0], $request[1]);
		$url = 'https://api.paybox.money/get_status.php';
		$data = $request;
			// use key 'http' even if you send the request to https://...
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($data)
			)
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		$check = $result;
		$xml = simplexml_load_string($result);
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);
		$payment_status = new PaymentStatus();
		$payment_status->pg_status = $array['pg_status'] ?? null;
		$payment_status->pg_payment_id = $array['pg_payment_id'] ?? null;
		$payment_status->pg_transaction_status = $array['pg_transaction_status'] ?? null;
		$payment_status->pg_can_reject = $array['pg_can_reject'] ?? null;
		$payment_status->pg_testing_mode = $array['pg_testing_mode'] ?? null;
		$payment_status->pg_captured = $array['pg_captured'] ?? null;
		$payment_status->pg_card_pan = $array['pg_card_pan'] ?? null;
		$payment_status->pg_create_date = $array['pg_create_date'] ?? null;
		$payment_status->pg_salt = $array['pg_salt'] ?? null;
		$payment_status->pg_sig = $array['pg_sig'] ?? null;
		$payment_status->save();
		if ($check === FALSE) {
			return false;
		}
		if($array['pg_status'] == 'ok' && $array['pg_transaction_status'] == 'ok'){
			return true;
		}
		return false;
	}
	catch (\Exception $e) {
		return false;
	}
}
public function test_all(Request $request)
{
	try {
		$user_id = Auth::user()->id;
		$items = Reservation::where([
			['user_id', $user_id],
			['status', 'reserved']
		])
		->with('lake', 'outcome_invites')->get();

		$items = $items->filter(function ($item) {
			return $item['lake'] != null;
		});

		$items = $items->map(function($item){
			$item->lake_services = json_decode($item->lake_services);
			$services = [];
			foreach ($item->lake_services as $ls_id) {
				$service_id = LakeServices::where('id', $ls_id)->first()->service_id;
				$services[] = Service::where('id', $service_id)->first();
			}
			$item['services'] = $services;
			return $item;
		});

		return response()->json($items->all(), 200);
	}
	catch (\Exception $e) {
		return response()->json(['message' => $e->getMessage()], 500);
	}
}
public function test_payment_url(Request $request) {
	DB::beginTransaction();
	try{
		$request = $request->all();
		$valid = $this->validatorUrl($request);
		if ($valid->fails()) {
			$jsonError = response()->json(['message' => $valid->errors()->all()], 400);
			return $jsonError;
		}
		$user = Auth::user();
		$reservation = Reservation::where('id', $request['reservation_id'])
		->with('outcome_invites')->first();
		if($reservation->user_id != $user->id){
			return response()->json(['message' => ['Резерв вам не принадлежит.']], 400);
		}
		if($reservation->status == 'paid'){
			return response()->json(['message' => ['Резерв уже оплачен.']], 400);
		}
		foreach ($reservation['outcome_invites'] as $o_invites) {
			if($o_invites['status'] != 'accepted'){
				return response()->json(['message' => ['Не все приглашения приняты.']], 400);
			}
		}

		$billed = 0;
		$lake = Lake::where('id', $reservation['lake_id'])->first();
		$billed += $lake->price*(sizeof($reservation['outcome_invites'])+1);
		foreach (json_decode($reservation['lake_services']) as $lake_service) {
			$ls = LakeServices::where('id', $lake_service)->first();
			if(empty($ls)){
				return response()->json(['message' => ['Услуга '.$lake_service.' не найдена.']], 400);
			}
			$billed += $ls->price;
		}
		$reservation->billed_amount = $billed;
		$reservation->save();

		$request = [
			'pg_merchant_id'=> 522248,
			'pg_order_id'=> $reservation->id,
			'pg_amount' => $reservation->billed_amount,
			'pg_currency' => 'KZT',
			'pg_request_method' => 'POST',
			'pg_result_url' => 'https://api.zapovednik-korgalzhyn.kz/api/process_result',
			'pg_success_url' => 'https://api.zapovednik-korgalzhyn.kz/api/success',
			'pg_success_url_method' => 'AUTOGET',
			'pg_failure_url' => 'https://api.zapovednik-korgalzhyn.kz/api/failure',
			'pg_failure_url_method' => 'AUTOGET',
			'pg_description' => $reservation->lake->name,
			'pg_user_phone' => $user->phone_number,
			'pg_language' => 'ru',
			'pg_salt' => 'zapovednik'.microtime(),
		];

		$request['pg_testing_mode'] = 1; //add this parameter to request for testing payments

		//if you pass any of your parameters, which you want to get back after the payment, then add them. For example:
		// $request['client_name'] = 'My Name';
		// $request['client_address'] = 'Earth Planet';
		//generate a signature and add it to the array
		ksort($request); //sort alphabetically
		array_unshift($request, 'payment.php');
		array_push($request, 'IPwavLfyKH3RT1yj'); //add your secret key (you can take it in your personal cabinet on paybox system)

		$request['pg_sig'] = md5(implode(';', $request));
		unset($request[0], $request[1]);
		$query = http_build_query($request);
		DB::commit();
		return response()->json(['url' => 'https://api.paybox.money/payment.php?'.$query], 200);
	}
	catch (\Exception $e) {
		DB::rollback();
		return response()->json(['message' => $e->getMessage()], 500);
	}
}
}
