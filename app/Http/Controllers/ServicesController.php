<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Service;
use Illuminate\Support\Facades\Validator;

class ServicesController extends Controller
{
    public function all()
	{
		try {
			$result = Service::all();
			return response()->json($result, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => [$e->getMessage()]], 500);
		}
	}

	protected function validatorC(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string'],
            'description' => ['nullable', 'string'],
        ]);
    }
	public function create(Request $request) {
		$request = $request->all();
		$valid = $this->validatorC($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
		try{
			$service = new Service();
			$service->name = $request['name'];
			$service->description = $request['description'];
			$service->save();

			return response()->json($service, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => [$e->getMessage()]], 500);
		}
	}

	protected function validatorU(array $data)
    {
        return Validator::make($data, [
            'id' => ['required', 'integer'],
            'name' => ['required', 'string'],
            'description' => ['nullable', 'string'],
        ]);
    }
	public function update(Request $request) {
		$request = $request->all();
		$valid = $this->validatorU($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
        DB::beginTransaction();
		try{
			$service = Service::where('id', $request['id'])->first();
			$service->name = $request['name'];
			$service->description = $request['description'];
			$service->save();
			DB::commit();
			return response()->json($service, 200);
		}
		catch (\Exception $e) {
			DB::rollback();
			return response()->json(['message' => [$e->getMessage()]], 500);
		}
	}

	protected function validatorD(array $data)
    {
        return Validator::make($data, [
            'id' => ['required', 'integer'],
        ]);
    }
	public function delete(Request $request) {
		$request = $request->all();
		$valid = $this->validatorD($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
		try{
			$service = Service::where('id', $request['id'])->first();

			if($service->delete()){
				$message = 'Запись была удалена!';
			}else{
				$message = 'Запись не была удалена!';
			}

			return response()->json(['message' => [$message]], 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => [$e->getMessage()]], 500);
		}
	}
}
