<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\IncomeInvite;
use App\OutcomeInvite;
use App\Reservation;
use App\User;
use App\Lake;
use Illuminate\Support\Facades\Validator;

class OutcomeInvitesController extends Controller
{
	protected function validatorA(array $data)
    {
        return Validator::make($data, [
            'reservation_id' => ['required', 'integer'],
        ]);
    }
    public function all(Request $request)
	{
		$request = $request->all();
		$valid = $this->validatorA($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);

            return $jsonError;
        }
		try {
			$result = OutcomeInvite::where([
				'reservation_id' => $request['reservation_id'],
								'inviter' => Auth::user()->id
			])->get();
			return response()->json($result, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorC(array $data)
    {
        return Validator::make($data, [
            'reservation_id' => ['required', 'integer'],
            'invited' => ['required', 'string', 'max:12'],
        ]);
    }
	public function create(Request $request) {
		$request = $request->all();
		$valid = $this->validatorC($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);

            return $jsonError;
        }
		DB::beginTransaction();
		try{
			$reservation = Reservation::where('id', $request['reservation_id'])->first();
			if(empty($reservation)){
				return response()->json(['message' => ['Резерв не найден.']], 400);
			}
			if($reservation->user_id != Auth::user()->id){
				return response()->json(['message' => ['Резерв вам не принадлежит.']], 400);
			}
			$phone_number = $this->check_number($request['invited']);
			if(!$phone_number){
				return response()->json(['message' => ['Не правильный номер.']], 400);
			}
			$user = User::where('phone_number', $phone_number)->first();
			if(empty($user)){
				return response()->json(['message' => ['Пользователь не найден.']], 400);
			}
			$out_inv = OutcomeInvite::where([
				['id', '=', $request['reservation_id']],
				['inviter', '=', Auth::user()->id],
				['invited', '=', $phone_number]
			])->first();
			if(!empty($out_inv)){
				return response()->json(['message' => ['Вы уже пригласили этого пользователья.']], 400);
			}
			$lake = Lake::where('id', $reservation->lake_id)->first();

			$outcome_invite = new OutcomeInvite();
			$outcome_invite->reservation_id = $request['reservation_id'];
			$outcome_invite->inviter = Auth::user()->id;
			$outcome_invite->invited = $request['invited'];
			$outcome_invite->invited_name = $user->last_name.' '.$user->first_name.' '.$user->parent_name;
			$outcome_invite->lake_name = $lake->name;
			$outcome_invite->save();

			$income_invite = new IncomeInvite();
			$income_invite->reservation_id = $request['reservation_id'];
			$income_invite->date_from = $reservation->date_from;
			$income_invite->lake_name = $lake->name;
			$income_invite->inviter = Auth::user()->id;
			$income_invite->inviter_first_name = Auth::user()->first_name;
			$income_invite->inviter_last_name = Auth::user()->last_name;
			$income_invite->invited = $request['invited'];
			$income_invite->outcome_invite = $outcome_invite->id;
			$income_invite->save();

			DB::commit();
			return response()->json($outcome_invite, 200);
		}
		catch (\Exception $e) {
			DB::rollback();
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	public function check_number($phone_number){
        $phone_number = trim($phone_number);
        if($phone_number[0] == '+'){
            $phone_number = substr($phone_number, 1);
        }
        if(strlen($phone_number) == 11){
            $phone_number = substr($phone_number, 1);
        }
        $phone_number = '7'.$phone_number;
        if(strlen($phone_number) != 11){
            return false;
        }
        return $phone_number;
    }

	protected function validatorD(array $data)
    {
        return Validator::make($data, [
            'id' => ['required', 'integer'],
        ]);
    }
	public function delete(Request $request) {
		$request = $request->all();
		$valid = $this->validatorD($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
        DB::beginTransaction();
		try{
			$outcome_invite = OutcomeInvite::where('id', $request['id'])->first();
			if(empty($outcome_invite)){
				return response()->json(['message' => ['Приглашение не найдено.']], 400);
			}
			if($outcome_invite->inviter != Auth::user()->id){
				return response()->json(['message' => ['Приглашение вам не принадлежит.']], 400);
			}

			$income_inv = IncomeInvite::where('outcome_invite', $outcome_invite->id)->first();
			if(!empty($income_inv)){
				$income_inv->delete();
			}
			$outcome_invite->delete();
			DB::commit();
			return response()->json(['message' => ['Запись была удалена!']], 200);
		}
		catch (\Exception $e) {
			DB::rollback();
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorALTR(array $data)
    {
        return Validator::make($data, [
            'reservation_id' => ['required', 'integer'],
            'invited' => ['required', 'string', 'max:12'],
        ]);
    }
	public function delete_altrv(Request $request) {
		$request = $request->all();
		$valid = $this->validatorALTR($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
        DB::beginTransaction();
		try{
			$phone_number = $this->check_number($request['invited']);
			if(!$phone_number){
				return response()->json(['message' => ['Не правильный номер.']], 400);
			}
			$outcome_invite = OutcomeInvite::where([
				['reservation_id', $request['reservation_id']],
				['invited', $phone_number],
			])->first();
			if(empty($outcome_invite)){
				return response()->json(['message' => ['Приглашение не найдено.']], 400);
			}
			if($outcome_invite->inviter != Auth::user()->id){
				return response()->json(['message' => ['Приглашение вам не принадлежит.']], 400);
			}

			$income_inv = IncomeInvite::where('outcome_invite', $outcome_invite->id)->first();
			if(!empty($income_inv)){
				$income_inv->delete();
			}
			$outcome_invite->delete();
			DB::commit();
			return response()->json(['message' => ['Запись была удалена!']], 200);
		}
		catch (\Exception $e) {
			DB::rollback();
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}
}