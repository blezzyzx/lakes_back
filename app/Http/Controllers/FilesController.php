<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\File;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class FilesController extends Controller
{
    protected function validatorU(array $data)
    {
        return Validator::make($data, [
            'category_id' => ['required', 'integer'],
            'file' => ['required', 'file'],
        ]);
    }
    public function upload(Request $request) {
    	DB::beginTransaction();
        $request = $request->all();
        $valid = $this->validatorU($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
        try {
            $file = $request['file'];

            if (empty($file)) {
                throw new \Exception('Файл не найден');
            }

            if (Auth::check()) {
                $exist_file = File::where(['user_id' => Auth::user()->id,
                    'category_id' => $request['category_id']])->first();
                if(!empty($exist_file)){
                    Storage::delete($exist_file->path);
                    $exist_file->delete();
                }
            }

            $fileName = md5($file-> getClientOriginalName() . microtime());
            $fileExt = $file->getClientOriginalExtension();
            $fileUrl = 'public/documents/' . $fileName . '.' . $fileExt;

            Storage::put($fileUrl, file_get_contents($file));

            $fileModel = new File();
            $fileModel->name = $file->getClientOriginalName();
            $fileModel->url = 'storage/documents/' . $fileName . '.' . $fileExt;
            $fileModel->path = $fileUrl;
            $fileModel->extension = $fileExt;
            $fileModel->content_type = $file->getClientMimeType();
            $fileModel->size = $file->getClientSize();
            $fileModel->hash = $fileName;
            $fileModel->category_id = $request['category_id'];
            $fileModel->save();
            DB::commit();

            return response()->json($fileModel, 200);
        } catch (\Exception $e) {
        	DB::rollback();
            return response()->json(['message' => ['Во время загрузки произошла ошибка'], 'error' => $e->getMessage()], 402);
        }
    }

    protected function validatorD(array $data)
    {
        return Validator::make($data, [
            'file_id' => ['required', 'integer'],
        ]);
    }
    public function delete(Request $request) {
    	DB::beginTransaction();
        $request = $request->all();
        $valid = $this->validatorD($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
        try {
            $file = File::where('id', $request['file_id'])->first();
            // return storage_path('app/' . $file->url);
            if (!$file) {
                throw new \Exception('Файл не найден');
            }
            if($file->user_id != null){
                if (Auth::check()) {
                    if($file->user_id != Auth::user()->id){
                        return response()->json(['message' => ['Файл вам не принадлежит.']], 402);
                    }
                }else{
                    return response()->json(['message' => ['Вы не авторизованы.']], 401);
                }
            }

            Storage::delete($file->path);
            $file->delete();
            DB::commit();

            return response()->json(['message' => ['Файл успешно удален']], 200);
        } catch (\Exception $e) {
        	DB::rollback();
            return response()->json(['message' => ['Не удалось удалить файл'],
        	'error' => $e->getMessage()], 402);
        }
    }
}