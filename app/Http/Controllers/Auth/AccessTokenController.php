<?php
namespace App\Http\Controllers\Auth;

use App\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Http\Response;
use \Laravel\Passport\Http\Controllers\AccessTokenController as ATC;

class AccessTokenController extends ATC
{
    public function check_number($phone_number){
        $phone_number = trim($phone_number);
        if($phone_number[0] == '+'){
            $phone_number = substr($phone_number, 1);
        }
        if(strlen($phone_number) == 11){
            $phone_number = substr($phone_number, 1);
        }
        $phone_number = '7'.$phone_number;
        if(strlen($phone_number) != 11){
            return false;
        }
        return $phone_number;
    }

  public function issueToken(ServerRequestInterface $request)
  {
    DB::beginTransaction();

    try {
        //get username (default is :email)
        $phone_number = $this->check_number($request->getParsedBody()['username']);
        $request->setSukaBlyat('username', $phone_number);
        //get user
        $my_user = User::where('phone_number', '=', $phone_number)->first();

        if (!$my_user) {
            throw new ModelNotFoundException();
        }

        $role = $my_user->role;

        DB::table('oauth_access_tokens')
        ->where('user_id', '=', $my_user->id)
        ->update(['revoked' => true]);

            //generate token
        $tokenResponse = parent::issueToken($request);

            //convert response to json string
        $content = $tokenResponse->getContent();

            //convert json to array
        $data = json_decode($content, true);

        if(isset($data["error"]))
            throw new OAuthServerException('Учетные данные пользователя были неверны', 6, 'Неверные данные', 402);

            //add access token to user
        $user = collect($my_user);
        $user->put('role', $role->name);
        $user->put('access_token', $data['access_token']);
        DB::commit();

        return response()->json($user);
    }
        catch (ModelNotFoundException $e) { // email notfound
            DB::rollback();
            return response(["message" => ["Пользователь не найден."]], 400);
        }
        catch (OAuthServerException $e) { //password not correct..token not granted
            DB::rollback();
            return response(["message" => ["Учетные данные пользователя были неверны."]], 400);
        }
        catch (Exception $e) {
            DB::rollback();
            return response(["message" => [$e->getMessage()]], 400);
        }
    }
}
