<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use App\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'parent_name' => ['nullable', 'string', 'max:255'],
            'phone_number' => ['required', 'string', 'max:12', 'unique:users'],
            'car_model' => ['nullable', 'string', 'max:255'],
            'car_number' => ['nullable', 'string', 'max:255'],
            'document_number' => ['required', 'string', 'max:12', 'unique:users'],
            'password' => ['required', 'string', 'min:3', 'confirmed'],
            'file_front_id' => ['nullable', 'integer'],
            'file_back_id' => ['nullable', 'integer'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function check_number($phone_number){
        $phone_number = trim($phone_number);
        if($phone_number[0] == '+'){
            $phone_number = substr($phone_number, 1);
        }
        if(strlen($phone_number) == 11){
            $phone_number = substr($phone_number, 1);
        }
        $phone_number = '7'.$phone_number;
        if(strlen($phone_number) != 11){
            return false;
        }
        return $phone_number;
    }

    protected function create(Request $request)
    {
        $request = $request->all();
        $request['phone_number'] = $this->check_number($request['phone_number']);
        $valid = $this->validator($request);

        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);

            return $jsonError;
        }
        $phone_number = $this->check_number($request['phone_number']);
        if(!$phone_number){
            return response()->json(['message' => ['Не правильный номер.']], 400);
        }

        DB::beginTransaction();

        try {
            $my_role = Role::USER;
            if(isset($request['godmode']) && $request['godmode'] == 'on'){
                $my_role = Role::ADMIN;
            }
            $user = User::create([
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'parent_name' => $request['parent_name'],
                'phone_number' => $phone_number,
                'car_model' => $request['car_model'],
                'car_number' => $request['car_number'],
                'document_number' => $request['document_number'],
                'password' => bcrypt($request['password']),
                'role_id' => $my_role,
            ]);
            $res = collect($user);

            if(isset($request['file_front_id']) && $request['file_front_id'] != null){
                $file_front = File::where('id', $request['file_front_id'])->first();
                if(empty($file_front)){
                    return response()->json(['message' => ['Файл '.$request['file_front_id'].' не существует.']], 400);
                }
                if($file_front->user_id != null){
                    return response()->json(['message' => ['Файл вам не принадлежит.']], 400);
                }
                $file_front->user_id = $user->id;
                $res['doc_front_url'] = $file_front->url;
                $file_front->save();
            }
            if(isset($request['file_back_id']) && $request['file_back_id'] != null){
                $file_back = File::where('id', $request['file_back_id'])->first();
                if(empty($file_back)){
                    return response()->json(['message' => ['Файл '.$request['file_back_id'].' не существует.']], 400);
                }
                if($file_back->user_id != null){
                    return response()->json(['message' => ['Файл вам не принадлежит.']], 400);
                }
                $file_back->user_id = $user->id;
                $res['doc_back_url'] = $file_back->url;
                $file_back->save();
            }

            $res['password'] = $request['password'];
            $res['visit_count'] = 0;
            $res['approve_status'] = 1;
            DB::commit();

            return response()->json($res);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['message' => [$e->getMessage()]], 500);
        }
    }
}
