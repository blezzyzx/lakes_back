<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Lake;
use App\Service;
use App\LakeServices;
use App\AppVersion;
use Illuminate\Support\Facades\Validator;

class LakesController extends Controller
{
	public function all()
	{
		try {
			$lakes = Lake::all();
			foreach ($lakes as $lake) {
				$services = Service::leftJoin('lake_services', 'services.id', 'lake_services.service_id')
					->where('lake_services.lake_id', $lake->id)
					->select('services.id as service_id','services.name', 'services.description', 'lake_services.id as lake_service_id', 'lake_services.price', 'lake_services.description as ls_description')
					->get();
				$lake['services'] = $services;
			}
			return response()->json($lakes, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	public function all_with_ver()
	{
		try {
			$lakes = Lake::all();
			foreach ($lakes as $lake) {
				$services = Service::leftJoin('lake_services', 'services.id', 'lake_services.service_id')
					->where('lake_services.lake_id', $lake->id)
					->select('services.id as service_id','services.name', 'services.description', 'lake_services.id as lake_service_id', 'lake_services.price', 'lake_services.description as ls_description')
					->get();
				$lake['services'] = $services;
			}
			$app_versions = AppVersion::first();
			return response()->json(['lakes' => $lakes, 'app_versions' => $app_versions], 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorVer(array $data)
    {
        return Validator::make($data, [
            'ios' => ['required', 'string', 'max:255'],
            'android' => ['required', 'string', 'max:255'],
        ]);
    }
	public function updateVersion(Request $request) {
		$request = $request->all();
		$valid = $this->validatorVer($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
		try{
			$app_versions = AppVersion::first();
			$app_versions->ios = $request['ios'];
			$app_versions->android = $request['android'];
			$app_versions->save();
			return response()->json($app_versions, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorC(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'price' => ['required', 'integer'],
        ]);
    }
	public function create(Request $request) {
		$request = $request->all();
		$valid = $this->validatorC($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
		try{
			$lake = new Lake();
			$lake->name = $request['name'];
			$lake->description = $request['description'];
			$lake->price = $request['price'];
			$lake->save();

			return response()->json($lake, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorU(array $data)
    {
        return Validator::make($data, [
        	'id' => ['required', 'integer'],
            'name' => ['required', 'string', 'max:255'],
            'price' => ['required', 'integer'],
        ]);
    }
	public function update(Request $request) {
		$request = $request->all();
		$valid = $this->validatorU($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
		try{
			$lake = Lake::where('id', $request['id'])->first();
			$lake->name = $request['name'];
			$lake->description = $request['description'];
			$lake->price = $request['price'];
			$lake->save();

			return response()->json($lake, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorD(array $data)
    {
        return Validator::make($data, [
            'id' => ['required', 'integer'],
        ]);
    }
	public function delete(Request $request) {
		$request = $request->all();
		$valid = $this->validatorD($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
		try{
			$lake = Lake::where('id', $request['id'])->first();

			if($lake->delete()){
				$message = 'Запись была удалена!';
			}else{
				$message = 'Запись не была удалена!';
			}

			return response()->json(['message' => [$message]], 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorC2(array $data)
    {
        return Validator::make($data, [
            'lake_id' => ['required', 'integer'],
            'service_id' => ['required', 'integer'],
            'price' => ['nullable', 'integer'],
        ]);
    }
	public function add_service(Request $request) {
		$request = $request->all();
		$valid = $this->validatorC2($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
		try{
			$lake_services = new LakeServices();
			$lake_services->service_id = $request['service_id'];
			$lake_services->lake_id = $request['lake_id'];
			$lake_services->price = $request['price'];
			$lake_services->description = $request['description'];
			$lake_services->save();

			return response()->json($lake_services, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorU2(array $data)
    {
        return Validator::make($data, [
            'lake_service_id' => ['required', 'integer'],
            'price' => ['nullable', 'string', 'max:11'],
        ]);
    }
	public function update_service(Request $request) {
		$request = $request->all();
		$valid = $this->validatorU2($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
		try{
			$lake_services = LakeServices::where('id', $request['lake_service_id'])->first();
			$lake_services->price = $request['price'];
			$lake_services->description = $request['description'];
			$lake_services->save();

			return response()->json($lake_services, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorD2(array $data)
    {
        return Validator::make($data, [
            'id' => ['required', 'integer'],
        ]);
    }
	public function delete_service(Request $request) {
		$request = $request->all();
		$valid = $this->validatorD2($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
		try{
			$lake_services = LakeServices::where('id', $request['lake_service_id'])->first();

			if($lake_services->delete()){
				$message = 'Запись была удалена!';
			}else{
				$message = 'Запись не была удалена!';
			}

			return response()->json(['message' => [$message]], 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}
}
