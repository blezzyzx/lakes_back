<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\IncomeInvite;
use App\OutcomeInvite;
use Illuminate\Support\Facades\Validator;

class IncomeInvitesController extends Controller
{
	public function check_number($phone_number){
        $phone_number = trim($phone_number);
        if($phone_number[0] == '+'){
            $phone_number = substr($phone_number, 1);
        }
        if(strlen($phone_number) == 11){
            $phone_number = substr($phone_number, 1);
        }
        $phone_number = '7'.$phone_number;
        if(strlen($phone_number) != 11){
            return false;
        }
        return $phone_number;
    }
    public function all()
	{
		try {
			$phone_number = $this->check_number(Auth::user()->phone_number);
			if(!$phone_number){
				return response()->json(['message' => ['Не правильный номер.']], 400);
			}
			$result = IncomeInvite::where('invited', $phone_number)->get();
			return response()->json($result, 200);
		}
		catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}

	protected function validatorU(array $data)
    {
        return Validator::make($data, [
        	'id' => ['required', 'integer'],
            'status' => ['required', 'string', 'max:10'],
        ]);
    }
	public function answer(Request $request) {
		$request = $request->all();
		$valid = $this->validatorU($request);
        if ($valid->fails()) {
            $jsonError = response()->json(['message' => $valid->errors()->all()], 400);
            return $jsonError;
        }
		DB::beginTransaction();
		try{
			$income_invite = IncomeInvite::where('id', $request['id'])->first();
			if(empty($income_invite)){
				return response()->json(['message' => ['Приглашение не найдено.']], 400);
			}
			$phone_number = $this->check_number(Auth::user()->phone_number);
			if(!$phone_number){
				return response()->json(['message' => ['Не правильный номер.']], 400);
			}
			if($income_invite->invited != $phone_number){
				return response()->json(['message' => ['Приглашение вам не принадлежит.']], 400);
			}
			$outcome_invite = OutcomeInvite::where('id', $income_invite->outcome_invite)->first();
			$outcome_invite->status = $request['status'];
			$outcome_invite->save();
			$income_invite->delete();
			DB::commit();
			return response()->json(['message' => ['Запись была обновлена!']], 200);
		}
		catch (\Exception $e) {
			DB::rollback();
			return response()->json(['message' => $e->getMessage()], 500);
		}
	}
}
