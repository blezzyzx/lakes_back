<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\File;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        try{
            DB::beginTransaction();
            $schedule->call(function () {
                $files = File::whereNull('user_id')->get();
                foreach ($files as $file) {
                    if(strtotime($file->created_at) + 86400 < time()){
                        $file->delete();
                    }
                }

                $inc_invites = IncomeInvite::all();
                foreach ($inc_invites as $inc_invite) {
                    if(strtotime($inc_invite->created_at) + 1800 < time()){
                        $outcome_invite = OutcomeInvite::where('id', $inc_invite->outcome_invite)->first();
                        $outcome_invite->delete();
                        $inc_invite->delete();
                    }
                }
            })->cron('* * * * *');
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
