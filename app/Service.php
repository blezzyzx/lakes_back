<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $fillable = [
        'name', 'description'
    ];

    public function lakes()
    {
        return $this->belongsToMany(Lake::class);
    }

    public function lake_services()
    {
        return $this->hasMany(LakeServices::class, 'service_id', 'id');
    }
}
