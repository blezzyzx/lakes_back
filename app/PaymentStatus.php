<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentStatus extends Model
{
    protected $fillable = [
        'pg_status', 'pg_payment_id', 'pg_transaction_status', 'pg_can_reject', 'pg_testing_mode', 'pg_captured', 'pg_card_pan', 'pg_create_date', 'pg_salt', '', 'pg_sig'
    ];
}