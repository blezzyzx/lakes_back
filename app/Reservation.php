<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'user_id', 'lake_id', 'lake_services', 'billed', 'date_from', 'date_to', 'status', 'entrance_approved'
    ];

	public function lake()
    {
        return $this->hasOne(Lake::class, 'id', 'lake_id');
    }

    public function outcome_invites()
    {
        return $this->hasMany(OutcomeInvite::class, 'reservation_id', 'id');
    }
}