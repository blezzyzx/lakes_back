<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	protected $fillable = [
        'name', 'path', 'url', 'hash', 'extension', 'content_type', 'size', 'user_id', 'category_id'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function category()
    {
        return $this->hasOne(FileCategory::class, 'id', 'category_id');
    }
}
