<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lake extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'description', 'price'
    ];

    public function services()
    {
        return $this->belongsToMany(
            Service::class,
            'lake_services',
            'lake_id',
            'service_id'
        );
    }

    public function reservation()
    {
        return $this->belongsToMany(Reservation::class);
    }
}
