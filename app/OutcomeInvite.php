<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutcomeInvite extends Model
{
    protected $fillable = [
        'reservation_id', 'inviter', 'invited', 'status', 'invited_name', 'lake_name'
    ];

    public function invited()
    {
        return $this->hasOne(User::class, 'phone_number', 'invited');
    }

    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }

    public function income_invite()
    {
        return $this->belongsTo(IncomeInvite::class);
    }
}
