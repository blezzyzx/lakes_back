<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LakeServices extends Model
{
    protected $fillable = [
        'service_id', 'lake_id', 'price', 'description'
    ];

    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }
}