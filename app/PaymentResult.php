<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentResult extends Model
{
	protected $fillable = [
        'pg_order_id', 'pg_payment_id', 'pg_amount', 'pg_currency', 'pg_net_amount', 'pg_ps_amount',
        'pg_ps_full_amount', 'pg_ps_currency', 'pg_payment_system', 'pg_description', 'pg_result', 'pg_payment_date', 'pg_can_reject', 'pg_user_phone', 'pg_need_phone_notification', 'pg_need_email_notification', 'pg_testing_mode', 'pg_captured', 'pg_card_pan', 'pg_card_exp', 'pg_card_owner', 'pg_auth_code', 'pg_card_brand', 'pg_salt', 'pg_sig'
    ];
}
